class Turtle:
  def __init__(self, color, legs):
    self.color = color
    self.legs = legs

Devin = Turtle("ginger", 4)
Graham = Turtle("dog-colored", 4)
Miguel = Turtle("brown", 3)

print(Devin.color)

class Dog:
  def __init__(self, name, color):
    self.name = name
    self.color = color

  def bark(self):
    print("Woof!")

Jio = Dog("Jio", "brown")
print(Jio.name)
Jio.bark()

class Dog:
  legs = 5
  def __init__(self, name, color):
    self.name = name
    self.color = color

Jio = Dog("Jio", "brown")
print(Jio.legs)
print(Dog.legs)

#Inherit

class Animal: 
  def __init__(self, name, color):
    self.name = name
    self.color = color

class Cat(Animal):
  def purr(self):
    print("Purr...")
        
class Dog(Animal):
  def bark(self):
    print("Woof!")

fido = Dog("Fido", "brown")
print(fido.color)
fido.bark()
