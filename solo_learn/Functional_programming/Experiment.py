numbernames = ['zero', \
        'one', 'two', 'three', \
        'four', 'five', 'six', \
        'seven', 'eight', 'nine', 'ten']

def recursion(x):
    try:
        nn = numbernames[x]
        length = len(nn)
        print(f'{nn} has {numbernames[length]} letters')
        if x == 4:
            print('THE END')
            return None
        return recursion(length)
    except:
        print('NO! You should have entered a number between 0 and 10')
print('Enter a number between 0 and 10 to see its length')
recursion(int(input()))

def makeaset():
    set = {2, ,3,4, ,5 ,6, ,7, 8, 9}

#ITERTOOLS

from itertools import count, accumulate, takewhile

def counttest():
    for i in count(3):
        print(i)
        if i >=11:
            break

def takewhiletest():
    nums = list(accumulate(range(8)))
    print(nums)
    print(list(takewhile(lambda x: x<= 6, nums)))

