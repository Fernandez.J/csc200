from unittest import TestCase

from polynomials import Polynomial

# Graham wants to use the polynomial module to do such:
# >>> p1 = '2x^2 + 2x + 2'
# >>> p2 = '7x^3+3x^2+x'
# >>> p1 + p2
# >>> '7x^3 + 6x^2 + 3x + 2'

class PolynomialTesterSuite(TestCase):

    def setUp(self):
        self.p1 = Polynomial()

    def test_default_polynomial_is_0(self):
        self.assertTrue(str(self.p1), '0')
    
    def test_create_constant_polynomials(self):
        self.p2 = Polynomial('42')
        self.assertTrue(str(self.p2), '42')
        
        

